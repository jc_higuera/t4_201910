package data_structures;

import java.util.Iterator;


public class LinkedList <T extends Comparable<T>> implements ILinkedList<T> 
{

	private Nodo<T> list;
	private int listSize;
	public LinkedList()
	{
		list = null;
	}

	public void addFirst(T item)
	{
		if(list!=null) {
			Nodo <T> newHead = new Nodo<T>(item, null, null);
			newHead.cambiarSiguiente(list);
			list = newHead;
			list.cambiarAnterior(newHead);
			listSize++;
		}else {
			list = new Nodo<T>(item, null, null);
		}
	}

	public void addAtEnd(T item) 
	{
		Nodo <T> newNodo = new Nodo<T>(item, null, null);
		if(list == null) {
			list = newNodo; 
		}
		else {
			Nodo <T> actual = list; 
			while(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			actual.cambiarSiguiente(newNodo);
			newNodo.cambiarAnterior(actual);
		} listSize++;
	}

	public void addAtK(int k, T item) throws java.lang.Exception
	{
		Nodo <T> newNodo = new Nodo<T>(item, null, null);
		if(k>listSize||k<0) {
			throw new Exception("K is not valid");
		}
		else if(k==0) {
			list = newNodo;
		}else {
			Nodo <T> actual = list; 
			int i = 1;
			while(k>i) {
				actual=actual.darSiguiente();
				i++;
			}
			actual.cambiarSiguiente(newNodo);
			newNodo.cambiarAnterior(actual);
		}
		listSize++;
	}

	public Nodo<T> getElement(int k)
	{

		if(list == null) {
			return null;
		}
		else if(k==0)
		{
			return list;
		}
		else 
		{
			Nodo <T> actual = list; 
			int i = 1;
			while(k>i) {
				actual=actual.darSiguiente();
				i++;
			}
			return actual;
		}
	}

	public Nodo<T> getCurrentElement()
	{
		if(list==null) {
			return null;
		}
		else {
			Nodo <T> actual = list; 
			while(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			return actual;
		}
	}

	public void delete()
	{
		if(list.darSiguiente()==null) {
			list = null;
		}else {
			Nodo <T> actual = list; 
			Nodo <T> aEliminar = list.darSiguiente();
			while(actual.darSiguiente().darSiguiente() != null) {
				actual = actual.darSiguiente();
				aEliminar = actual.darSiguiente().darSiguiente();
			}
			actual.cambiarSiguiente(null);
			aEliminar.cambiarAnterior(null);
		}
	}

	public void deleteAtK(int k)
	{
		if(k==0) {
			Nodo<T> aEliminar = list;
			list = aEliminar.darSiguiente();
			list.cambiarAnterior(null);
			aEliminar.cambiarSiguiente(null);
		}else {
			Nodo <T> actual = list; 
			int i = 1;
			while(k>i) {
				actual=actual.darSiguiente();
				i++;
			}
			Nodo<T> aEliminar = actual.darSiguiente();
			actual.cambiarSiguiente(aEliminar.darSiguiente());
			actual.darSiguiente().cambiarAnterior(actual);
			aEliminar.cambiarSiguiente(null);
			aEliminar.cambiarAnterior(null);
		}
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> iterator = new Iterator<T>() {
			Nodo<T> act = null;
			public boolean hasNext() {
				if (listSize == 0) {
					return false;
				}
				else {
					if (act == null) {
						return true;
					}else {
						return act.darSiguiente() != null;
					}
				}
			}
			public T next() {
				if (act == null) {
					act = list;
				}else {
					act = act.darSiguiente();
				}
				return act.darData();

			}
			public boolean hasPrevious() {
				if (listSize == 0) {
					return false;
				}
				else {
					if (act == null) {
						return true;
					}else {
						return act.darAnterior() != null;
					}
				}
			}
			public T previous()
			{
				if (act == null) {
					act = list;
				}else {
					act = act.darAnterior();
				}
				return act.darData();
			}
		};
		return iterator;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return listSize;
	}

}
