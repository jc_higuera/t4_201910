package model.util;

public class Sort {

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo ShellSort
		int N = datos.length;
		int h = 1;
		while(h<N/3) h = 3*h+1;
		while(h>=1) {
			for (int i = h; i < N; i++) {
				for (int j = i ; j >= h && less(datos[j], datos[j-h]) ; j-=h) {
					exchange(datos, j, j-h);
				}
			}
			h = h/3;
		}

	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) {
		int n = datos.length;
		if (n < 2) {
			return;
		}
		int mid = n / 2;
		Comparable[] l = new Comparable[mid];
		Comparable[] r = new Comparable[n - mid];

		for (int i = 0; i < mid; i++) {
			l[i] = datos[i];
		}
		for (int i = mid; i < n; i++) {
			r[i - mid] = datos[i];
		}
		ordenarMergeSort(l);
		ordenarMergeSort(r);

		merge(datos, l, r, mid, n - mid);
	}
	public static void merge(Comparable[] a, Comparable[] l, Comparable[] r, int left, int right) {

		int i = 0, j = 0, k = 0;
		while (i < left && j < right) {
			if (less(l[i], r[j])) {
				a[k++] = l[i++];
			}
			else {
				a[k++] = r[j++];
			}
		}
		while (i < left) {
			a[k++] = l[i++];
		}
		while (j < right) {
			a[k++] = r[j++];
		}
	}
	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) {

		// TODO implementar el algoritmo QuickSort
		sort(datos, 0, datos.length - 1);
	}
	private static void sort(Comparable[] datos, int lo, int hi) {
		if (hi <= lo) return;
		int j = partition(datos, lo, hi);
		sort(datos, lo, j-1); // Sort left part a[lo .. j-1].
		sort(datos, j+1, hi); // Sort right part a[j+1 .. hi].
	}



	private static int partition(Comparable[] a, int lo, int hi)
	{ 
		int i = lo, j = hi+1; 
		Comparable v = a[lo]; 
		while (true)
		{ 
			while (less(a[++i], v)) if (i == hi) break;
			while (less(v, a[--j])) if (j == lo) break;
			if (i >= j) break;
			exchange(a, i, j);
		}
		exchange(a, lo, j); 
		return j; 
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		// TODO implementar
		return v.compareTo(w)<0;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// TODO implementar
		Comparable t = datos[i];
		datos[i] = datos[j];
		datos[j] = t;

	}

}
