package model.vo;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	// TODO Definir los atributos de una infraccion

	private int id;
	private String location;
	private int adressId;
	private String streetSegId;
	private String xCoord;
	private String yCoord;
	private String ticketType;
	private int fine;
	private int totalPaid;
	private int penalty1;
	private String accident;
	private String issueDate;
	private String violationCode;
	private String violationDesc;
	private int rowId;




	public VOMovingViolation(String id, String location, String adressId, String streetSegId, String xCoord, String yCoord,
			String ticketType, String fine, String totalPaid, String penalty1, String accident, String issueDate,
			String violationCode, String violationDesc, String rowId) 
	{
		this.id = Integer.parseInt(id);

		this.location = location;
		if(!adressId.equals("")) {
			this.adressId = Integer.parseInt(adressId);
		}else if(adressId.equals("")) {
			this.adressId =0;
		}
		this.streetSegId = streetSegId;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.ticketType = ticketType;
		if(!fine.equals("")) {
			this.fine = Integer.parseInt(fine);
		}else if(fine.equals("")) {
			this.fine =0;
		}
		if(!totalPaid.equals("")) {
			this.totalPaid = Integer.parseInt(totalPaid);
		}else if(totalPaid.equals("")) {
			this.totalPaid =0;
		}if(!penalty1.equals("")) {
			this.penalty1 = Integer.parseInt(penalty1);
		}else if(penalty1.equals("")) {
			this.penalty1 =0;
		}
		this.accident = accident;
		this.issueDate = issueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
		if(!rowId.equals("")) {
			this.rowId = Integer.parseInt(rowId);
		}else if(rowId.equals("")) {
			this.rowId =0;
		}

	}

	public int objectId() {
		return id;
	}

	public String getLocation() {
		return location;
	}
	public int getAdressId() {
		return adressId;
	}
	public String getStreetSegId() {
		return streetSegId;
	}
	public String getxCoord() {
		return xCoord;
	}
	public String getyCoord() {
		return yCoord;
	}
	public String getTicketType() {
		return ticketType;
	}
	public int getFine() {
		return fine;
	}
	public int getPenalty1() {
		return penalty1;
	}

	public String getAccidentIndicator() {
		return accident;
	}

	public String getTicketIssueDate() {
		return issueDate;
	}
	public String getViolationCode() {
		return violationCode;
	}
	public String getViolationDescription() {
		return violationDesc;
	}
	public int getRowId() {
		return rowId;
	}

	public int getTotalPaid() {

		return totalPaid;
	}

	public GregorianCalendar getDate() {
		String[] a = issueDate.split("T");
		String[] b = a[0].split("-");
		String[] c = a[1].split(":");
		GregorianCalendar ans = new GregorianCalendar(Integer.parseInt(b[0]), Integer.parseInt(b[1]), Integer.parseInt(b[2]), Integer.parseInt(c[0]), Integer.parseInt(c[1]));
		return ans;

	}


	@Override
	public int compareTo(VOMovingViolation o) {
		// TODO implementar la comparacion "natural" de la clase
		int ans = 0;
		if(this.issueDate.equals(o.issueDate)) {
			if(this.id<o.id) {
				ans = -1;
			}else if(this.id>o.id) {
				ans=1;
			}else {
				ans = 0;
			}
		}else {
			if(this.getDate().compareTo(o.getDate())>0) {
				ans=1;
			}else if(this.getDate().compareTo(o.getDate())<0) {
				ans=-1;
			}

		}
		return ans;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return id+issueDate;
	}
}
